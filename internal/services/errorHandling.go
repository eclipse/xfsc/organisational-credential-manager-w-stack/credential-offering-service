package services

import (
	"errors"

	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/credential-verification-service/internal/common"
)

type ServerErrorResponse struct {
	Message string `json:"message"`
}

func ErrorResponse(c *gin.Context, err string, exception error) error {
	log := common.GetEnvironment().GetLogger()
	if exception != nil {
		log.Error(exception, "Detailed Error: "+exception.Error())
	} else {
		log.Error(errors.New(err), err)
	}

	c.JSON(400, ServerErrorResponse{
		Message: err,
	})
	return errors.New(err)
}

func InternalErrorResponse(c *gin.Context, err string, exception error) error {
	log := common.GetEnvironment().GetLogger()
	if exception != nil {
		log.Error(exception, "Detailed Error: "+exception.Error())
	} else {
		log.Error(errors.New(err), err)
	}

	c.JSON(500, ServerErrorResponse{
		Message: err,
	})
	return errors.New(err)
}
